**Changement effectués: **


virementController.java : --> @RestController("versement") -> @RestController @RequestMapping("versement") 

--> Nommer les attributs d'une manière claire 

--> changer equals par '==' 

--> Enlever intValue(), : on travaille avec BigDecimal ( ( '<' , '>' ) --> compareTo(), + --> add()..) 

--> placer la fonction save() dans la couche service 

**En général: **


**lisibilité : ** 


-Supprimer les getter et setter et remplacer par @Data -Des attributs comprénsible  

**Bonnes pratiques: **


- Avoir une couche service pour utiliser l'annotation @Transactional lorque il est nécéssaire 

-Avoir des interfaces et leurs implémentation 

-Modifier les paths des fonctions selon la nomination convenable du Rest

 -Définir les erreurs sous forme d’un POJO pour informer le consommateur de l’API REST 
