package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;



public class VersementDto {
	
	  private String rib;
    public String getMotif() {
		return motif;
	}
	public void setMotif(String motif) {
		this.motif = motif;
	}
	private String motif;
	  private BigDecimal montantVersement;
	  private Date date;
	  private String nomPrenom;
	
	  
	 
	public String getNomPrenom() {
		return nomPrenom;
	}
	public void setNomPrenom(String nomPrenom) {
		this.nomPrenom = nomPrenom;
	}
	 
	 
	public String getRib() {
		return rib;
	}
	public void setRib(String rib) {
		this.rib = rib;
	}
	public BigDecimal getMontantVersement() {
		return montantVersement;
	}
	public void setMontantVersement(BigDecimal montantVersement) {
		this.montantVersement = montantVersement;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
