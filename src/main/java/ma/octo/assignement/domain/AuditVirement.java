package ma.octo.assignement.domain;

import ma.octo.assignement.domain.util.EventType;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "AUDIT_VIREMENT")
public @Data class AuditVirement {
  
@Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;

  
 
}