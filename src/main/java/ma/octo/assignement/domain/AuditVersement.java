package ma.octo.assignement.domain;

import ma.octo.assignement.domain.util.EventType;



import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "AUDIT_VERSEMENT")
public @Data class AuditVersement {
   
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;

   

   
}
