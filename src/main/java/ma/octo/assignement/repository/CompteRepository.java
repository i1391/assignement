package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


public interface CompteRepository extends JpaRepository<Compte, Long> {
  Compte findByNrCompte(String nrCompte);
  Compte findByRib(String rib);

}
