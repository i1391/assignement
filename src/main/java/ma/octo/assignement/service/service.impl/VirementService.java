package ma.octo.assignement.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.VirementService;

@Service
@Transactional
public class VirementServiceImpl implements VirementService{

	 @Autowired
	 private VirementRepository virementRepo;
	 
	@Override
	public List<Virement> loadAll() {
		 
		 List<Virement> all = virementRepo.findAll();

	        if (CollectionUtils.isEmpty(all)) {
	            return null;
	        } else {
	            return all;
	        }
	    }

	@Override
	public void ajouterVirement(Virement virement) {
		 virementRepo.save(virement);
		
	}
	

}
