package ma.octo.assignement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.VersementService;

//Une transaction doit vérifier les propriétés
// ACID (atomicité, consistance, isolation, durabilité
//toutes les actions sontentièrement exécutées ou qu’elles n’ontaucun effet.
//chaque transaction amènera le système d'un état valide à un autre état valide
//La propriété d'isolation assure que l'exécutionsimultanée de transactions produit le même état que celui qui serait obtenu par l'exécution en série des transactions.
//les modifications sont définitives et entièrement visibles par le reste des applications
@Service
@Transactional
public class VersementServiceImpl implements VersementService {

	 @Autowired
	 private VersementRepository versementRepo;
	 
	@Override
	public List<Versement> loadAllVersement() {
		 
		 List<Versement> all = versementRepo.findAll();

	        if (CollectionUtils.isEmpty(all)) {
	            return null;
	        } else {
	            return all;
	        }
	    }

	@Override
	public void ajouterVersement(Versement versement) {
		versementRepo.save(versement);
		
	}
	
	
}
