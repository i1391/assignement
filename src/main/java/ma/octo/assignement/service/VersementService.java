package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Versement;



public interface VersementService {
	
	public List<Versement> loadAllVersement();
	public void ajouterVersement(Versement versement);
}
