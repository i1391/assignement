package ma.octo.assignement.exceptions;

import org.springframework.transaction.annotation.Transactional;


public class TransactionException extends Exception {

  private static final long serialVersionUID = 1L;
  
   
  public TransactionException() {
  }

  public TransactionException(String message) {
    super(message);
    System.out.println(message);
  }
}
