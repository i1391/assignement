package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.VirementService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

// la valeur du mapping se fait dans Request mapping 
//RestControllr est cosidéré comme un component
@RestController
@RequestMapping("virements")

public class VirementController {
	
	public static final int MONTANT_MAXIMAL = 10000;
	public static final int MONTANT_MINIMAL = 10;
	
	 Logger LOGGER = LoggerFactory.getLogger(VirementController.class);
	 
// changer les noms des attributs
	 
	 @Autowired
	 private CompteRepository compteRepo;
	 
	 @Autowired
	 private VirementService virementService;
	 
	 @Autowired
	 private UtilisateurRepository userRepo;
	 
	 @Autowired
	 private AutiService monservice;
	 
	 
	 @GetMapping("lister_comptes")
	    public List<Compte> loadAlll() {
		 List<Compte> all =compteRepo.findAll();
		 if (CollectionUtils.isEmpty(all)) {
	            return null;
	        } else {
	            return all;
	        }
	    }
	 
	 
	 
	 @GetMapping("lister_virements")
	    List<Virement> loadAll() {
	        List<Virement> all = virementService.loadAll();

	        if (CollectionUtils.isEmpty(all)) {
	            return null;
	        } else {
	            return all;
	        }
	    }
	 
	 
	 
	 @GetMapping("lister_utilisateurs")
	    List<Utilisateur> loadAllUtilisateur() {
	        List<Utilisateur> all = userRepo.findAll();

	        if (CollectionUtils.isEmpty(all)) {
	            return null;
	        } else {
	            return all;
	        }
	    }
	 
	 @PostMapping("/executerVirements")
	    @ResponseStatus(HttpStatus.CREATED)
	    public void createTransaction(@RequestBody VirementDto virementDto)
	            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
	    	
	          Compte cptEmett = compteRepo.findByNrCompte(virementDto.getNrCompteEmetteur());
	          Compte cptBenef = compteRepo.findByNrCompte(virementDto.getNrCompteBeneficiaire());

	         if (cptEmett == null || cptBenef == null) {
	        	 //delete
	           // System.out.println("Compte Non existant");
	             throw new CompteNonExistantException("Compte Non existant");
	        }
	         
	         
	       //  The reason you can't use BigDecimal#equals() is that it takes scale into consideration:
           //  new BigDecimal("0").equals(BigDecimal.ZERO) // true
	       //  new BigDecimal("0.00").equals(BigDecimal.ZERO) // false
	          ////
	        //On supprime intValue parceque ona un montant en BigDecimal
			// Montant
	        if (virementDto.getMontantVirement() == null || virementDto.getMontantVirement().compareTo(BigDecimal.ZERO) == 0) {
	            throw new TransactionException("Montant vide");
	         } 
	        	
	 
	        int a=(virementDto.getMontantVirement()).compareTo(BigDecimal.valueOf(MONTANT_MINIMAL));
	       // if 1 st 2  
	         if ( a == -1 )
	        {
	             // on supprime le syso: répétition
	            throw new TransactionException("Montant minimal de virement non atteint");
	        }
	         //if 1 bt 2
	         int b =(virementDto.getMontantVirement()).compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL));
	        
	         if ( b == 1 )
		        {
	        	 //supprimer : répétiton 
	        	  // System.out.println("Montant maximal de virement dépassé");
		            throw new TransactionException("Montant maximal de virement dépassé");
		        }
	 		
	         
	 		 //Motif
	        //lenghth=0 et n'ont pas < 0, : motif est string , sinon ne sera pas prix en considération
	        //

	        if (virementDto.getMotif().length() == 0 ) {
	            System.out.println("Motif vide");
	            throw new TransactionException("Motif vide");
	        }
	        
	        // solde
	        int c =(cptEmett.getSolde()).compareTo(virementDto.getMontantVirement());

	        	
           if (c == -1) {
        	 
	            throw new SoldeDisponibleInsuffisantException("Solde  Insuffisant ");
	        }

           	// enregistrer les modification
	        cptEmett.setSolde(cptEmett.getSolde().subtract(virementDto.getMontantVirement()));
	        compteRepo.save(cptEmett);
	        
	        
	       
	        // we use the add method
	        cptBenef.setSolde((cptBenef.getSolde()).add(virementDto.getMontantVirement()));
	        compteRepo.save(cptBenef);
	        
	  
	        Virement virement = new Virement();
	        virement.setDateExecution(virementDto.getDate());
	        virement.setCompteBeneficiaire(cptBenef);
	        virement.setCompteEmetteur(cptEmett);
	        virement.setMontantVirement(virementDto.getMontantVirement());

	        virementService.ajouterVirement(virement);
	        
	        monservice.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                    .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                    .toString());
}

	   

}
