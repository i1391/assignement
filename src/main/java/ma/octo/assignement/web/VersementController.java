package ma.octo.assignement.web;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.VersementService;

@RestController
@RequestMapping("versement")
public class VersementController {
	 public static final int MONTANT_MAXIMAL = 10000;
	public static final int MONTANT_MINIMAL = 10;

	    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);
	    
	    @Autowired
	    private VersementService versementService;
	    
	    
	    
	    @Autowired
	    private CompteRepository compteRepo;
	    
	    @Autowired
		 private AutiService monservice;
	    
	  
	    
		 
	    @GetMapping("versements")
		    List<Versement> loadAll() {
		        List<Versement> all = versementService.loadAllVersement();

		        if (CollectionUtils.isEmpty(all)) {
		            return null;
		        } else {
		            return all;
		        }
		    }
	    

	    @PostMapping("/versements")
	    @ResponseStatus(HttpStatus.CREATED)
	    public void createTransaction(@RequestBody VersementDto versementDto)
	            throws  CompteNonExistantException, TransactionException {
	    	
	    	
	        Compte cptBenef = compteRepo.findByRib(versementDto.getRib());

	        if ( cptBenef == null) {
	            
	            throw new CompteNonExistantException("Compte Non existant");
	        }

//	        if (virementDto.getMontantVirement() == BigDecimal.ZERO) {
//	            System.out.println("Montant vide");
//	            throw new TransactionException("Montant vide");
//	        }  else 
	        
	        //
           int a=(versementDto.getMontantVersement()).compareTo(BigDecimal.valueOf(MONTANT_MINIMAL));
		       // if 1 st 2  
	         if ( a == -1 )
	        {
	             // on supprime le syso: répétition
	            throw new TransactionException("Montant minimal de virement non atteint");
	        }
	         //if 1 bt 2
	         int b =(versementDto.getMontantVersement()).compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL));
	        
	         if ( b == 1 )
		        {
	        	 //supprimer : répétiton 
	        	  // System.out.println("Montant maximal de virement dépassé");
		            throw new TransactionException("Montant maximal de virement dépassé");
		        }
	         
	         //

//	        if (virementDto.getMotif().length() < 0) {
//	            System.out.println("Motif vide");
//	            throw new TransactionException("Motif vide");
//	        }

//	        if (cptEmett.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
//	            LOGGER.error("Solde insuffisant pour l'utilisateur");
//	        }


	        //cptEmett.setSolde(cptEmett.getSolde().subtract(virementDto.getMontantVirement()));
	       // compteService.ajouterCompte(cptEmett);
    Versement versement = new Versement();
	        cptBenef.setSolde((cptBenef.getSolde()).add(versementDto.getMontantVersement()));
	        compteRepo.save(cptBenef);
	        
	  
	       
	        versement.setDateExecution(versementDto.getDate());
	        versement.setCompteBeneficiaire(cptBenef);
	        versement.setNom_prenom_emetteur(versementDto.getNomPrenom());
	        versement.setMontantVersement(versementDto.getMontantVersement());
	        versement.setMotifVersement(versementDto.getMotif());
	     
	       
	         
	        versementService.ajouterVersement(versement);
	        
	        monservice.auditVersement("Versement depuis " + versementDto.getNomPrenom()+ " vers " + versementDto.getRib()
                   + " d'un montant de " +versementDto.getMontantVersement().toString());
}

	   


}
