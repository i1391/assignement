package ma.octo.assignement.web.common;


import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

 //Pour informer le consommateur de l’API REST des erreurs, il faut lui envoyer
//les informations de celle-ci dans la réponse sous le format d’échange utilisé
//(JSON)

@ControllerAdvice
public class ExceptionHandelingController {

    @ExceptionHandler(SoldeDisponibleInsuffisantException.class)
    public ResponseEntity<ErrorResponse> handleException(SoldeDisponibleInsuffisantException exc) {
    	
    	// create a ErrorResponse

    	ErrorResponse error = new ErrorResponse();

    	error.setStatus(HttpStatus.NOT_FOUND.value());
    	error.setMessage(exc.getMessage());
    	error.setTimeStamp(System.currentTimeMillis());
        return new ResponseEntity<>(error, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
    }

    
    
    @ExceptionHandler(CompteNonExistantException.class)
    public ResponseEntity<ErrorResponse> handleException(CompteNonExistantException exc) {
    	// create a ErrorResponse

    	ErrorResponse error = new ErrorResponse();

    	error.setStatus(HttpStatus.NOT_FOUND.value());
    	error.setMessage(exc.getMessage());
    	error.setTimeStamp(System.currentTimeMillis());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
    
    
    
    // add Transaction exception 
    @ExceptionHandler(TransactionException.class)
    public ResponseEntity<ErrorResponse> handleException(TransactionException ex) {
    	// create a ErrorResponse

    	ErrorResponse error = new ErrorResponse();

    	error.setStatus(HttpStatus.NOT_FOUND.value());
    	error.setMessage(ex.getMessage());
    	error.setTimeStamp(System.currentTimeMillis());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}
