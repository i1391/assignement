package ma.octo.assignement.web.common;

public class ErrorResponse {
// on définit l’erreur sous forme d’un POJO avec les attributs
//	nécessaires et Spring va convertir ce POJO en JSON
	private int status;
	private String message;
	private long timeStamp;

	public ErrorResponse() {
		
	}

	public ErrorResponse(int status, String message, long timeStamp) {
		this.status = status;
		this.message = message;
		this.timeStamp = timeStamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}	
	
}
